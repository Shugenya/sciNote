import org.junit.*;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotSelectableException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Random;
import java.util.concurrent.TimeUnit;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SciNoteTest {
    private static WebDriver driver;
    private static WebDriverWait wait;
    private String URL;
    private static String projectName;
    private static String newTaskName;

    //Default timeout for waiting elements to load
    private static final int DEFAULT_TIMEOUT = 30;


    @BeforeClass
    public static void setup(){
        //Open driver
        System.setProperty("webdriver.chrome.driver", "browserDrivers/chromedriver.exe");
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, DEFAULT_TIMEOUT);

    }

    @AfterClass
    public static void tearDown(){
        //Close driver
        driver.close();
    }

    @Test
    public void step1_loginTest() {
        //Load properties
        SciNoteProperties sciNoteProperties = new SciNoteProperties();
        URL = sciNoteProperties.getURL();
        String username = sciNoteProperties.getUserName();
        String password = sciNoteProperties.getPassword();

        //Open sciNote
        driver.get(URL);

        //Login
        WebElement emailInput = driver.findElement(By.id("user_email"));
        WebElement passwordInput = driver.findElement(By.id("user_password"));
        WebElement loginButton = driver.findElement(By.name("commit"));
        emailInput.sendKeys(username);
        passwordInput.sendKeys(password);
        loginButton.click();

        //Verify login
        String expectedTitle = "SciNote | Home";
        String actualTitle = driver.getTitle();
        Assert.assertEquals("Page title after login", expectedTitle, actualTitle);

    }

    @Test
    public void step2_newProjectTest() {
        //Click new project link
        WebElement newProjectLink = driver.findElement(By.xpath("//div[@id='projects-toolbar']/form/div/a"));
        wait.until(ExpectedConditions.elementToBeClickable(newProjectLink));
        newProjectLink.click();

        //New project details
        projectName = "My lab project " + randomInt();
        WebElement projectNameField = driver.findElement(By.id("project_name"));
        projectNameField.sendKeys(projectName);
        WebElement createBtn = driver.findElement(By.name("commit"));
        createBtn.click();

        //Verify new project exists
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(projectName)));
        Boolean projectExists = driver.findElement(By.linkText(projectName)).isDisplayed();
        Assert.assertTrue("New project is displayed", projectExists);

    }

    @Test
    public void step3_newExperimentTest(){
        //Open existing project
        WebElement openExperiment = driver.findElement(By.linkText(projectName));
        openExperiment.click();

        //Click new experiment button
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("new-experiment")));
        WebElement newExperimentButton = driver.findElement(By.id("new-experiment"));
        wait.until(ExpectedConditions.elementToBeClickable(newExperimentButton));
        newExperimentButton.click();

        //New experiment details
        By experimentNameCondition = By.id("experiment-name");
        wait.until(ExpectedConditions.visibilityOfElementLocated(experimentNameCondition));
        WebElement newExperimentName = driver.findElement(experimentNameCondition);
        String experimentName = "My experiment " + randomInt();
        newExperimentName.sendKeys(experimentName);
        WebElement createBtn = driver.findElement(By.name("commit"));
        createBtn.click();

        //Verify new experiment was created
        wait.until(ExpectedConditions.urlContains("/experiments/"));
    }

    @Test
    public void step4_newTaskTest(){
        //Edit experiment
        driver.findElement(By.id("edit-canvas-button")).click();

        //Create new task
        By newTaskCondition = By.id("canvas-new-module");
        wait.until(ExpectedConditions.elementToBeClickable(newTaskCondition));
        Actions action =new Actions(driver);
        action.dragAndDrop(driver.findElement(newTaskCondition), driver.findElement(By.id("diagram-container"))).build().perform();

        //New task details
        String taskNameValue = "Task name";
        newTaskName = "Test task " + randomInt();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(taskNameValue)));
        driver.findElement(By.name(taskNameValue)).sendKeys(newTaskName);
        driver.findElement(By.xpath("//button[contains(text(), 'Add')]")).click();

        //Verify new task was created
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[text()='" + newTaskName + "']")));

        //Save the task
        driver.findElement(By.id("canvas-save")).submit();
    }

    @Test
    public void step5_newStepTest(){
        //Open task
        driver.findElement(By.id("diagram-container")).findElement(By.linkText(newTaskName)).click();

        //Add new step
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("fluid-content")));
        //additional timeout to solve the problem with sometimes failing test as New task button is not loaded yet
        //It's a temporary solution, that will be improved
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS) ;
        driver.findElement(By.id("fluid-content")).findElement(By.cssSelector("a.btn.btn-primary")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS) ;

        //Step details
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("step_name")));
        driver.findElement(By.cssSelector("input#step_name")).sendKeys("First step");
        driver.findElement(By.xpath("//input[@value='Add']")).click();

        //Verify step was created
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("step-number")));
    }

    @Test
    public void step6_addDataToTask(){
        //Edit the task
        driver.findElement((By.cssSelector("a[data-action='edit-step']"))).click();

        //Add new checklist
        By checkListPath = By.xpath("//*[@id='new-step-checklists-tab']/a");
        wait.until(ExpectedConditions.visibilityOfElementLocated(checkListPath));
        driver.findElement(checkListPath).click();
        driver.findElement(By.xpath("//*[@id='new-step-checklists']/a")).click();

        //Checklist details
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("step_checklists_attributes_0_name")));
        driver.findElement(By.id("step_checklists_attributes_0_name")).sendKeys("My new checklist");
        driver.findElement(By.cssSelector("a.add_nested_fields_link")).click();
        driver.findElement(By.id("step_checklists_attributes_0_checklist_items_attributes_0_text")).sendKeys("First item on list");

        //Save data
        driver.findElement(By.xpath("//input[@value='Save']")).click();
    }

    @Test
    public void step7_logout(){
        //Click account icon
        try {
            WebElement menuIcon = driver.findElement(By.className("navbar-toggle"));
            if(menuIcon.isDisplayed()){
                menuIcon.click();
            }
        }
        catch (ElementNotSelectableException e){
            //No navigation bar, we will select account icon directly
        }

        By accountSelector = By.cssSelector("a[title=Account]");
        wait.until(ExpectedConditions.visibilityOfElementLocated(accountSelector));
        driver.findElement(accountSelector).click();

        //Logout
        By logoutSelector = By.xpath("//*[@id='user-account-dropdown']/ul/li[1]/a");
        wait.until(ExpectedConditions.visibilityOfElementLocated(logoutSelector));
        driver.findElement(logoutSelector).click();

        //Verify user is logged out
        wait.until(ExpectedConditions.urlContains("/users/sign_in"));

    }
    private int randomInt(){
        return new Random().nextInt(9999);
    }
}
