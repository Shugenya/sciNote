import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

public class SciNoteProperties {
    private static String SCI_NOTE_URL;
    private static String SCI_NOTE_USERNAME;
    private static String SCI_NOTE_PASSWORD;

    public SciNoteProperties(){
        try {
            readPropValues();
        } catch (IOException e) {
                   }
    }

    public void readPropValues() throws IOException {
        Properties prop = new Properties();
        String propFileName = "config.properties";
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        if (inputStream != null) {
            prop.load(inputStream);
        } else {
            throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
        }

        SCI_NOTE_URL = prop.getProperty("URL");

        SCI_NOTE_USERNAME = prop.getProperty("username");
        SCI_NOTE_PASSWORD = prop.getProperty("password");

    }

    public String getURL(){
        return SCI_NOTE_URL;
    }

    public String getUserName(){
        return SCI_NOTE_USERNAME;
    }

    public String getPassword(){
        return SCI_NOTE_PASSWORD;
    }

}
